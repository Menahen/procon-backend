export class Produto {
    private titulo: string;
    private descricao: string;
    private preco: number;

    constructor(titulo: string, descricao: string, preco: number) {
        this.titulo = titulo;
        this.descricao = descricao;
        this.preco = preco;
    }

}