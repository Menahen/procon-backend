import { Pool } from "pg";

export class DAOConnection {

    pool: Pool = null;

    private create(): Pool {
        let config = {
            user: 'postgres',
            database: 'produtos',
            password: '123456',
            host: 'localhost',
            port: 5432,
            max: 10,
            idleTimeoutMillis: 30000,
        };
        return new Pool(config);
    }

    constructor() {
        this.pool = this.create();
    }

    public getAllProducts(callback) {
        this.pool.connect(function (err, client, done) {
            if (err) {
                return console.error('Não foi possivel conectar ao banco de dados', err);
            }
            client.query('select * from produtos', function (err, result) {
                if (err) {
                    callback(result);
                    return console.error('error running query', err);
                } else{
                    console.log("Resultado da query: " + JSON.stringify(result.rows));
                    callback(result);
                    return result.rows;
                }
            });
        });
         return null;
    }

}