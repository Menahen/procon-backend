import { NextFunction, Request, Response, Router } from "express";
import { DAOConnection } from "../dao/DAOConnection";
import { Produto } from "../entities/Produto";

export class Produtos {

    public static create(router: Router) {
        console.log("[ProdutosRoute::create] Creating produtos route.");
        router.get("/produtos", (req: Request, res: Response, next: NextFunction) => {
            new Produtos().getAllProdutos(req, res, next);
        });
    }

    public getAllProdutos(req: Request, res: Response, next: NextFunction) {
        let conn = new DAOConnection();
        conn.getAllProducts(function(produtos){
            let produtosObject: Produto[] = null;
            console.log("Produtos retornados pelo banco: " + JSON.stringify(produtos));
            produtosObject = produtos.rows;
            res.json(produtosObject);
        });
    }

}