import { NextFunction, Request, Response, Router } from "express";

export class RotaTeste {

    public static create(router: Router) {
        console.log("[IndexRoute::create] Creating teste route.");

        router.get("/proximo", (req: Request, res: Response, next: NextFunction) => {
            res.write("<h1>Hello</h1>");
            next();
        });
         router.get("/proximo", (req: Request, res: Response, next: NextFunction) => {
            res.write("<h1>World</h1>");
            res.end();
        });

        router.get("/teste", (req: Request, res: Response, next: NextFunction) => {
            new RotaTeste().teste(req, res, next);
        });

         router.get("/teste/dois", (req: Request, res: Response, next: NextFunction) => {
            new RotaTeste().testeDois(req, res, next);
        });
    }

    public teste(req: Request, res: Response, next: NextFunction) {
        res.locals.BASE_URL = "/";
        res.json({mensagem: "teste", nome: "filipe"});
    }

     public testeDois(req: Request, res: Response, next: NextFunction) {
        res.locals.BASE_URL = "/";
        res.json({mensagem: "teste2", nome: "ligia"});
    }

}