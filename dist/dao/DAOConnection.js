"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const pg_1 = require("pg");
class DAOConnection {
    constructor() {
        this.pool = null;
        this.pool = this.create();
    }
    create() {
        let config = {
            user: 'postgres',
            database: 'produtos',
            password: '123456',
            host: 'localhost',
            port: 5432,
            max: 10,
            idleTimeoutMillis: 30000,
        };
        return new pg_1.Pool(config);
    }
    getAllProducts(callback) {
        this.pool.connect(function (err, client, done) {
            if (err) {
                return console.error('Não foi possivel conectar ao banco de dados', err);
            }
            client.query('select * from produtos', function (err, result) {
                if (err) {
                    callback(result);
                    return console.error('error running query', err);
                }
                else {
                    console.log("Resultado da query: " + JSON.stringify(result.rows));
                    callback(result);
                    return result.rows;
                }
            });
        });
        return null;
    }
}
exports.DAOConnection = DAOConnection;
