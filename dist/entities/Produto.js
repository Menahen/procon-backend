"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Produto {
    constructor(titulo, descricao, preco) {
        this.titulo = titulo;
        this.descricao = descricao;
        this.preco = preco;
    }
}
exports.Produto = Produto;
