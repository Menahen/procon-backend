"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class RotaTeste {
    static create(router) {
        console.log("[IndexRoute::create] Creating teste route.");
        router.get("/proximo", (req, res, next) => {
            res.write("<h1>Hello</h1>");
            next();
        });
        router.get("/proximo", (req, res, next) => {
            res.write("<h1>World</h1>");
            res.end();
        });
        router.get("/teste", (req, res, next) => {
            new RotaTeste().teste(req, res, next);
        });
        router.get("/teste/dois", (req, res, next) => {
            new RotaTeste().testeDois(req, res, next);
        });
    }
    teste(req, res, next) {
        res.locals.BASE_URL = "/";
        res.json({ mensagem: "teste", nome: "filipe" });
    }
    testeDois(req, res, next) {
        res.locals.BASE_URL = "/";
        res.json({ mensagem: "teste2", nome: "ligia" });
    }
}
exports.RotaTeste = RotaTeste;
