"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const DAOConnection_1 = require("../dao/DAOConnection");
class Produtos {
    static create(router) {
        console.log("[ProdutosRoute::create] Creating produtos route.");
        router.get("/produtos", (req, res, next) => {
            new Produtos().getAllProdutos(req, res, next);
        });
    }
    getAllProdutos(req, res, next) {
        let conn = new DAOConnection_1.DAOConnection();
        conn.getAllProducts(function (produtos) {
            let produtosObject = null;
            console.log("Produtos retornados pelo banco: " + JSON.stringify(produtos));
            produtosObject = produtos.rows;
            res.json(produtosObject);
        });
    }
}
exports.Produtos = Produtos;
